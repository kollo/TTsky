<img alt="TTsky" src="logo/sky.png"/>

TTsky for TomTom
================

(c) Markus Hoffmann 2008-2011

A simple sky atlas, showing the positions and rise and set times of sun, moon 
and 100 nearest stars.

    Developer:   Markus Hoffmann 
    Version:     1.01  (20.01.2008)
    Architecture: any
    Language:    X11-Basic
    Comment:     It is written in X11-Basic,therefor it needs this to beinstalled.
    DependenciesRequires: TTconsole, X11-BasicSee also: Software



Description
-----------

TTsky is a simple sky atlas, showing position and riseand set times of sun, 
moon and 100 Stars.
A Sun and Moon calendar. The Sunset, Sunrise, Moonset, Moonrise, Transits, 
Moonphases, disdances and positions on the sky are calculated based on the 
actual GPS time and position which we get from the ttn application via file 
interface. This could in future be extended to calculate and show the rise 
and set times, as well as the exact positions on the sky of all planets, 
comets etc. The picture view could also be enlaged to have a more detailed 
view including stars. 
Then, the TomTom becomes a valuable tool for (hobby)-astronomers who want 
to exactly position their telescopes to the objects of interest. This is the 
only other killer application of a TomTomdevice other that the car navigation 
I can imagine.

### Install

Unpack the binary package (zip file) and move the contents to your tomtom 
directory just as they are. Thats it. You can use the device with navigation 
software as before. The icon for TTsky should appear in the settings menu.

### Screenshots

<div style="display:flex;">
<img alt="App image" src="screenshots/screenshot1.png" width="50%">
</div>


### Important Note:

    TTsky is free software and comes with NO WARRANTY - read the file
    COPYING for details
    
    (Basically that means, free, open source, use and modify as you like, don't
    incorporate it into non-free software, no warranty of any sort, don't blame me
    if it doesn't work.)
    

You need to have X11-Basic and TTconsole installed to use this application 
on your TomTom device. 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

